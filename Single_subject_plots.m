%% 19.05.2020
%  Scripts to plot the ERPs distribution. For the moment, it works with groups 
%  with 2 levels only. 
%
%
%  DATA PREPARATION: 
%
%  1) in the folder that contains the script, create a
%  folder with the datafile to load (i.e., the grand-average ERP of each
%  subject). Each file should be formatted as a space-separated numbers
%  (dimension: 64 rows X Ntimepoints). Name each file in sequential order
%  (e.g.: CRFSAEEG002_average.asc for sbj1, CRFSAEEG008_average.asc for sbj2 etc
%  etc) making sure that the length of the first part is the same between
%  subjects files(e.g.: length(CRFSAEEG002) = length(CRFSAEEG112)).  
% 
%  2) Create an excel file (e.g., GROUP.xlsx) with the first coloumn the
%  subject's ID (e.g., CRFSAEEG002, CRFSAEEG008, etc ... ), and the second
%  column the group's level name (e.g., 'HA' or 'LA'). Place the file in
%  the same folder as this script
%
%  3) In the folder containing this script, ceate a folder for saving the figures
%
%  4) Place the BIOSEMI 64_Biosemi_elec.mat matlab file in the same folder
%  containing this script
%
%  5) This script needs the function plotshaded.m

%  Dr. Paolo Ruggeri - UNIL (pool.ruggeri@gmail.com)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% OPTIONS: MODIFY BELOW

% ERP parameters
FS = 512;                   % sampling frequency in Hz
TOI_i = -100;               % lower boundary of the period of interest (in ms): for the plot
TOI_f = 700;                % upper boundary of the period of interest (in ms): for the plot
onset = -200;               % onset of the original raw data
ROI_1 = {'PO4','PO8','O2'}; % electrodes to include in the ROI 1
ROI_2 = {'Fz','FCz','F1'};  % electrodes to include in the ROI 2
normalize_GFP = 1;          % 1 = yes; 0 = no
n_el = 64;                  % number of electrodes used for measurements
group_name = {'LA','HA'};   % Name of the group's level. These must be the same as the one contained in the second column of the group's level names
length_IDname = 11;         % length of the subject's ID (e.g.: length(CRFSAEEG002) == 11). NB: all the subject's ID must have the same length

% input and output folders
inputFolder = ('/Users/pruggeri/Documents/Lavoro/Research/CRFSAEEG/FIGURE_ERPs/preprocessed_data');

% folder path for saved figures
outputFigures = ('/Users/pruggeri/Documents/Lavoro/Research/CRFSAEEG/FIGURE_ERPs/Figures');

% filname path containin group's level info
inputFilename = ('/Users/pruggeri/Documents/Lavoro/Research/CRFSAEEG/FIGURE_ERPs/GROUP.xlsx');

% plot options
COLOR_PLOT(1,:) = [0.8500, 0.3250, 0.0980]; % color assigned for the first group's level
COLOR_PLOT(2,:) = [0, 0.4470, 0.7410];      % color assigned for the second group's level

% plot dimensions 
DIM(1) = 20; % horizontal dimension
DIM(2) = 2;  % vertical dimension

%% DO NOT MODIFY BELOW:

% load electrode disposition
load('64_Biosemi_elec.mat');
Channel = extractfield(elec64,'labels');

% load file with group info
opts = spreadsheetImportOptions("NumVariables", 2);

opts.VariableNames = ["ID_participant", "group_name"];
opts.VariableTypes = ["string", "string"];
opts = setvaropts(opts, [1, 2], "WhitespaceRule", "preserve");
opts = setvaropts(opts, [1, 2], "EmptyFieldRule", "auto");
GROUP = readtable("/Users/pruggeri/Documents/Lavoro/Research/CRFSAEEG/FIGURE_ERPs/GROUP.xlsx", opts, "UseExcel", false);
GROUP = table2array(GROUP);

% get participant's data
data_names = dir(inputFolder); data_names = extractfield(data_names,'name'); data_names = data_names(3:end);

cont_G1 = 0;
cont_G2 = 0;
for nsubj = 1:length(data_names)
    
    temp = load([inputFolder filesep data_names{nsubj}]);
    
    if normalize_GFP
        temp = temp./repmat(sqrt(sum(temp.^2,1)),n_el,1)*sqrt(n_el);
    end
    
    
    str_ID = data_names{nsubj}(1:length_IDname);
    
    str_GROUP = GROUP(strcmp(str_ID,GROUP(:,1)),2);
    
    if strcmp(group_name{1},str_GROUP)
        cont_G1 = cont_G1 + 1;
        Data_G1(:,:,cont_G1) = temp;
    else
        cont_G2 = cont_G2 + 1;
        Data_G2(:,:,cont_G2) = temp;
    end
    
end

%  parameters for exctraction of periods of interest
dt = 1000/FS;      % delta t in ms
t = onset:dt:(onset+dt*(size(Data_G1,2)-1));   % time axis in ms
[~,IND_i] = min(abs(t-TOI_i)); % index corresponding to the initial TOI_i
[~,IND_f] = min(abs(t-TOI_f)); % index corresponding to the final TOI_f

% take the TOI 
Data_G1 = Data_G1(:,IND_i:IND_f,:);
Data_G2 = Data_G2(:,IND_i:IND_f,:);








%----------------------------- PLOTS --------------------------------------

% plot ROI 1

nel_ROI = length(ROI_1);

for nch = 1:nel_ROI
    ind_ch(nch) = find(strcmp(Channel,ROI_1{nch}) == 1);
end

Data_G1_ROI = squeeze(mean(Data_G1(ind_ch',:,:),1));
Data_G2_ROI = squeeze(mean(Data_G2(ind_ch',:,:),1));
 
Data_G1_ROI_mean = mean(Data_G1_ROI,2);
Data_G2_ROI_mean = mean(Data_G2_ROI,2);
 
Data_G1_ROI_sem = std(Data_G1_ROI,0,2)/sqrt(size(Data_G1_ROI,2));
Data_G2_ROI_sem = std(Data_G2_ROI,0,2)/sqrt(size(Data_G2_ROI,2));

figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(t(IND_i:IND_f),Data_G1_ROI_mean,'Color',COLOR_PLOT(1,:),'Linewidth',2); hold on
plot(t(IND_i:IND_f),Data_G2_ROI_mean,'Color',COLOR_PLOT(2,:),'Linewidth',2)
plotshaded(t(IND_i:IND_f),[(Data_G1_ROI_mean+Data_G1_ROI_sem)';(Data_G1_ROI_mean-Data_G1_ROI_sem)'],COLOR_PLOT(1,:))
plotshaded(t(IND_i:IND_f),[(Data_G2_ROI_mean+Data_G2_ROI_sem)';(Data_G2_ROI_mean-Data_G2_ROI_sem)'],COLOR_PLOT(2,:))

grid on
ylim([min([Data_G1_ROI_mean;Data_G2_ROI_mean]-0.5) max([Data_G1_ROI_mean;Data_G2_ROI_mean]+0.5)])
xlim([TOI_i TOI_f])
% Create xlabel
xlabel({'Time [ms]'},'FontSize',14);
ylabel({'Potential [\muV]'},'FontSize',14);
set(axes1,'FontSize',14,'XAxisLocation','top','YTick',[-6:0.5:6]);
view(axes1,[-0.185066478823672 -90]);
box(axes1,'on');
% Set the remaining axes properties
set(axes1,'FontSize',14);
% title
title(['ROI 1'])
% set the final dimensions
set(gcf,'units','centimeters','innerposition',[10 10 10+DIM(1) 10+DIM(2)])
set(gcf,'units','centimeters','outerposition',[10 10 10+DIM(1) 10+DIM(2)])
 
legend(group_name{1},group_name{2},'Location','NorthEast')









% plot ROI 2

nel_ROI = length(ROI_2);

for nch = 1:nel_ROI
    ind_ch(nch) = find(strcmp(Channel,ROI_2{nch}) == 1);
end

Data_G1_ROI = squeeze(mean(Data_G1(ind_ch',:,:),1));
Data_G2_ROI = squeeze(mean(Data_G2(ind_ch',:,:),1));
 
Data_G1_ROI_mean = mean(Data_G1_ROI,2);
Data_G2_ROI_mean = mean(Data_G2_ROI,2);
 
Data_G1_ROI_sem = std(Data_G1_ROI,0,2)/sqrt(size(Data_G1_ROI,2));
Data_G2_ROI_sem = std(Data_G2_ROI,0,2)/sqrt(size(Data_G2_ROI,2));

figure2 = figure;

% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');

plot(t(IND_i:IND_f),Data_G1_ROI_mean,'Color',COLOR_PLOT(1,:),'Linewidth',2); hold on
plot(t(IND_i:IND_f),Data_G2_ROI_mean,'Color',COLOR_PLOT(2,:),'Linewidth',2)
plotshaded(t(IND_i:IND_f),[(Data_G1_ROI_mean+Data_G1_ROI_sem)';(Data_G1_ROI_mean-Data_G1_ROI_sem)'],COLOR_PLOT(1,:))
plotshaded(t(IND_i:IND_f),[(Data_G2_ROI_mean+Data_G2_ROI_sem)';(Data_G2_ROI_mean-Data_G2_ROI_sem)'],COLOR_PLOT(2,:))

grid on
ylim([min([Data_G1_ROI_mean;Data_G2_ROI_mean]-0.5) max([Data_G1_ROI_mean;Data_G2_ROI_mean]+0.5)])
xlim([TOI_i TOI_f])
% Create xlabel
xlabel({'Time [ms]'},'FontSize',14);
ylabel({'Potential [\muV]'},'FontSize',14);
set(axes2,'FontSize',14,'XAxisLocation','top','YTick',[-6:0.5:6]);
view(axes2,[-0.185066478823672 -90]);
box(axes2,'on');
% Set the remaining axes properties
set(axes2,'FontSize',14);
% title
title(['ROI 2'])
% set the final dimensions
set(gcf,'units','centimeters','innerposition',[10 10 10+DIM(1) 10+DIM(2)])
set(gcf,'units','centimeters','outerposition',[10 10 10+DIM(1) 10+DIM(2)])
 
legend(group_name{1},group_name{2},'Location','NorthEast')








%% save figures

saveas(figure(1),[outputFigures,'/ROI_1'],'epsc');
saveas(figure(2),[outputFigures,'/ROI_2'],'epsc');



