%% Perform the average potential within a time interval of interest, after the L2 normalization of the potential. 
%
%  This script performs the following:
%  1) automatically loads txt (or asc) files contained the input folder (that the user must create).
%  NB: Each file contains a grand average ERP. I suggest to create one
%  folder for each condition you want to compare (e.g., if you want to
%  compare manuel NOGO vs visual NOGO within a specified period of
%  interest, create a input Folder and put inside the files of all the
%  participants for the manual and visual NOGO)
%  The files should display values of potential in the following matrix
%  format: [Numelec x TimeFrames]
%
%  2) normalize each time frame for the L2 norm
%
%  3) Average the ERP inside the user defined Time of Interest (TOI),
%  defined in ms
%
%  4) Save the output file in the corresponding output folder. The output
%  file is a column vector, with length = number of electrodes
%
% Example: 
% inputFolder = ('/Users/pruggeri/Documents/Lavoro/Softwares/PersonalScripts/Utils_EEG/DataToConvert'); -> files that you want to normalize and average are inside the folder DataToConvert
% outputFolder = ('/Users/pruggeri/Documents/Lavoro/Softwares/PersonalScripts/Utils_EEG/DataConverted'); -> the script saves output files inside the folder DataConverted
% onset = -500;  % onset of the ERP in ms
% fs = 512;      % sampling frequency in Hz
% TOI_i = 250;   % lower boundary of the period of interest (in ms)
% TOI_f = 325;   % upper boundary of the period of interest (in ms)
%
% Dr. Paolo Ruggeri, UNIL - Switzerland. September 2019
% pool.ruggeri@gmail.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all

%% Options: User must modifiy these options

% input and output folders
inputFolder = ('/Users/pruggeri/Documents/Lavoro/Softwares/PersonalScripts/Utils_EEG/DataToConvert');
outputFolder = ('/Users/pruggeri/Documents/Lavoro/Softwares/PersonalScripts/Utils_EEG/DataConverted');

% parameters
onset = -500;  % onset of the ERP in ms
fs = 512;      % sampling frequency in Hz
TOI_i = 400;   % lower boundary of the period of interest (in ms)
TOI_f = 500;   % upper boundary of the period of interest (in ms)


%% Don't modifiy script below. 

% Check file names in the inputfolder
pairs = {};
listing = dir(inputFolder);
for nlist = 1:size(listing,1)
    if ~listing(nlist).isdir
        pairs = [ pairs listing(nlist).name ];
    end
end
npairs = length(pairs);

for i= 1:npairs
    pairsId = pairs{i};
    
    inputDataset  = [ inputFolder filesep pairsId];
    outputDataset = [ outputFolder filesep 'GFPnorm_' 'Avg_' num2str(TOI_i) '_' num2str(TOI_f) 'ms_' pairsId ];
    
    %% load dataset
    Data = load(inputDataset);
    
    %% L2 normalization
    L2_Data = sqrt(sum(Data.^2,1));
    Data = Data./repmat(L2_Data,64,1)*sqrt(64);
    
    %% define parameters for exctraction of periods of interest
    dt = 1000/fs;      % delta t in ms
    t = onset:dt:(onset+dt*(size(Data,2)-1));   % time axis in ms

    %% get index of the TOI and average ERP between the TOI 
    [~,Ii] = min(abs(t-TOI_i));
    [~,If] = min(abs(t-TOI_f));    
    Data_avg = mean(Data(:,Ii:If),2);
    
    %% Export data to file
    dlmwrite(outputDataset,Data_avg)
    
end

